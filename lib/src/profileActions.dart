import 'dart:async';

import 'package:upnp/upnp.dart';
import 'package:dslink/dslink.dart';

import 'package:em_dslink_wemo/src/devicesForWemoLightSwitch.dart' as wemo_device;
import 'package:em_dslink_wemo/src/wemoLightSwitchNode.dart' as wemo_ls_node;
import 'package:em_dslink_wemo/src/verifyInputs.dart' as verify;
import 'package:em_dslink_wemo/src/customEventSupport.dart' as events;
import 'package:em_dslink_wemo/src/globals.dart' as globals;
import 'package:http/http.dart' as http;

import 'package:em_dslink_wemo/src/wemoCallsMapped.dart' as wemo_calls;
import 'package:em_dslink_wemo/src/profileActionHelpers.dart' as helper;
import 'package:petitparser/json.dart' as petit_parser;

///////////////////////////
// Remove is always first!
///////////////////////////

/// [###] Removed existing node from structure
Map<String,dynamic> remove(String pathOfParent, LinkProvider link) {

  bool response = false;

//  LocalNode lNode = link.getNode(pathOfParent);
//  String uuid = lNode.getAttribute('@uuid');

  ///TODO: need to un-subscribe/kill timers if it exists with appropriate ifs

  try {

    link.removeNode(pathOfParent);
    link.save();

    response = true;

  } catch (e) {

    return {
      'succeeded' : response,
    };

  }

  return {
    'succeeded' : response,
  };
}

///////////////////////////
// Root Profiles!
///////////////////////////

/// [DONE] map search for upnp enabled devices
Future<Map<String,dynamic>> discoverDevices(String pathOfParent, LinkProvider link) async {

  int numberFound = 0;
  int numberAdded = 0;
  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  bool inDiscovery = lNode.getAttribute('@inDiscovery').toString() == 'true';

  Map<String,Device> foundLightSwitches;

  if (inDiscovery) {

    return {    };

  } else {

    lNode.attributes['@inDiscovery'] = true;
    lNode.listChangeController.add('@inDiscovery');
    foundLightSwitches = await wemo_device.getDevicesList().whenComplete( ()
    {

      lNode.attributes['@inDiscovery'] = false;
      lNode.listChangeController.add('@inDiscovery');

    });

  }

  numberFound = foundLightSwitches.length;

  lNode.children.values.forEach( (node)
  {
    if (node.get(r'$is') == 'deviceRoot') {
      String knownUuid = node.get('@uuid');
      foundLightSwitches.remove(knownUuid);
    }
  }
  );

  if (foundLightSwitches.isNotEmpty) {

    foundLightSwitches.forEach( (uuid, device) async {

      wemo_ls_node.WeMoNode newWemoNode = new wemo_ls_node.WeMoNode(device.uuid, deviceIn: device);
      newWemoNode.addNodeToTree(link);
      await newWemoNode.updatePointValues(link);

      numberFound++;

    });
  }

  succeeded = true;

  return {
    'succeeded'   : succeeded,
    'numberFound' : numberFound,
    'numberAdded' : numberAdded
  };

}

/// [DONE] search within given IP ranges
Future<Map<String,dynamic>> searchInRange(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  int numberFound = 0;
  int numberAdded = 0;
  bool succeeded = false;

  String startIp = params['startIP'];
  String endIp = params['endIP'];

  // check that these IP are valid, also checking for wildcards,
  if (!verify.validIpRange(startIp, endIp)) {

    return {
      'numberFound' : numberFound,
      'numberAdded' : numberAdded,
      'succeeded' : succeeded
    };

  }

  // from here on get the root requirements on ip search...
  LocalNode lNode = link.getNode(pathOfParent);
  int ipBatchSearchSize = int.parse(lNode.get('@ipBatchSearchSize').toString());

  // if already in discovery exit "searchInRange" otherwise start it
  if (lNode.getAttribute('@inDiscovery').toString() == 'true') {

    return {
      'numberFound' : numberFound,
      'numberAdded' : numberAdded,
      'succeeded' : succeeded
    };

  } else {

    lNode.attributes['@inDiscovery'] == true;
    lNode.listChangeController.add('@friendlyName');

  }

  // get the defaults needed for discovery
  List<dynamic> listOfPorts = lNode.attributes['@defaultPorts'];
  List<dynamic> listOfSubnets = lNode.attributes['@defaultSubnets'];

  // from here we have checked and verified that everything is good to go.
  List<String> listOfIps = helper.getListOfIps(startIp, endIp, listOfSubnets);

  // at this point we have all possible IPs and can start searching algorithm, this will also take in a batch size for the IPs
  List<Device> foundDevices = await wemo_device.searchForDevicesInList(listOfIps, listOfPorts, ipBatchSearchSize);

  // get all the existing UUIDs that are already present
  List<String> existingUuidList = helper.getListOfExistingUuid(link);

  numberFound = foundDevices.length;

  // remove device if its alrdy found.....
  foundDevices.forEach( (Device device) async {
    if (!existingUuidList.contains(device.uuid)) {
      wemo_ls_node.WeMoNode newWemoNode =  new wemo_ls_node.WeMoNode(device.uuid, deviceIn: device);
      newWemoNode.addNodeToTree(link);
      await newWemoNode.updatePointValues(link);
      numberAdded++;
    }
  });

  succeeded = true;

  return {
    'numberFound' : numberFound,
    'numberAdded' : numberAdded,
    'succeeded' : succeeded
  };


}

/// [DONE] call to scan network and fix nodes that may have incorrect IPs, will fail if call changes nothing
Future<Map<String,dynamic>> rebaseNodeAddresses({LinkProvider link, String pathOfParent, Map<String, dynamic> params}) async {

  bool succeeded = false;
  int nodesUpdated = 0;

  LocalNode rootNode = link.getNode('/');

  List<String> recoveryIpRange = rootNode.get('@recoveryIpRange');

  List<dynamic> listOfPorts = rootNode.get('@defaultPorts');
  List<dynamic> listOfSubnets = rootNode.get('@defaultSubnets');
  int ipBatchSearchSize = int.parse(rootNode.getAttribute('@ipBatchSearchSize').toString());

  String startIp = recoveryIpRange.first;
  String endIp = recoveryIpRange.last;

  // get the full list of IPs from the recovery and default values
  List<String> listOfIps = helper.getListOfIps(startIp, endIp, listOfSubnets);

  // search for all devices in the above
  List<Device> foundDevicesList = await wemo_device.searchForDevicesInList(listOfIps, listOfPorts, ipBatchSearchSize);

  // get all the existing UUIDs that are already present
  List<String> existingUuidList = helper.getListOfExistingUuid(link);

  for (Device foundDevice in foundDevicesList) {

    if (existingUuidList.contains(foundDevice.uuid)) {

      // get node values
      LocalNode wemoNode = link.getNode('/${foundDevice.uuid}');

      bool subscription = (wemoNode.get('@subscription').toString().toLowerCase() == 'true');
      String nodeIp = wemoNode.get('@ipAddress');
      String nodePort = wemoNode.get('@port').toString();

      // get devices values
      List<String> brokenAddressFromDevice = foundDevice.urlBase.split('/')[2].split(':');
      String deviceIp = brokenAddressFromDevice[0];
      String devicePort = brokenAddressFromDevice[1];

      // if port or ip are wrong then change the values in the node
      if (nodeIp != deviceIp || nodePort != devicePort) {

        wemoNode.attributes['@ipAddress'] = deviceIp;
        wemoNode.listChangeController.add('@ipAddress');

        wemoNode.attributes['@port'] = devicePort;
        wemoNode.listChangeController.add('@port');

        link.save();

        if (subscription) {
          await wemo_ls_node.updateNodePointValues(link, foundDevice.uuid);
        }

        nodesUpdated++;

      }

    }

  }

  (nodesUpdated != 0) ? succeeded = true : succeeded = false;

  return {
    'devicesFound'  : foundDevicesList.length,
    'nodesUpdated'  : nodesUpdated,
    'succeeded'     : succeeded
  };

}

/// [DONE] add device from specific ip (will use specified ports of interest to connect)
Future<Map<String,dynamic>> addDevice(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  List<String> ports = link.getNode(pathOfParent).attributes['@defaultPorts'];

  String ip = params['ip'];

  List<String> existingSwitches = [];

  link.getNode(pathOfParent).children.forEach( (String nodeName, Node node) {
    if(node.get(r'$is') == 'deviceRoot') {
      existingSwitches.add(nodeName);
    }
  });

  if(!verify.validIp(ip)){
    return {
      'succeeded' : succeeded,
    };

  };

  Device foundDevice = await wemo_device.searchForDeviceInIP(ip, ports)
    .catchError( (e) {

    });

  if (foundDevice != null) {
    wemo_ls_node.WeMoNode newWemoNode =  new wemo_ls_node.WeMoNode(foundDevice.uuid, deviceIn: foundDevice);
    newWemoNode.addNodeToTree(link);
    await newWemoNode.updatePointValues(link);
    succeeded = true;
  }

  return {
    'succeeded' : succeeded,
  };

}

Future<Map<String,dynamic>> toggleTimedDeviceDiscovery({String pathOfParent, Map<String, dynamic> params, LinkProvider link}) async {

  bool response = false;

  LocalNode lNode = link.getNode(pathOfParent);
  bool currentStateTimedDeviceDiscovery = (lNode.get('@toggleTimedDeviceDiscovery').toString().toLowerCase() == 'true');

  lNode.attributes['@toggleTimedDeviceDiscovery'] = !currentStateTimedDeviceDiscovery;
  lNode.listChangeController.add('@toggleTimedDeviceDiscovery');

  try {

    link.save();
    response = true;

  } catch (e) {

    return {
      'succeeded': response
    };

  }

  return {
    'succeeded' : response
  };

}

/// [DONE] from LS root, change root defaults for new LS nodes
Future<Map<String,dynamic>> changeRootDefaults(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  petit_parser.JsonParser jsonParser = new petit_parser.JsonParser();

  try {
    // (Array of int) ports (will use petit json parser for parsing json strings)
    var newDefaultPorts = params['defaultPorts'];
    var newDefaultSubnets = params['defaultSubnets'];

    if (params['defaultPorts'] is String) {
      newDefaultPorts = jsonParser.parse(newDefaultPorts).value;
    }

    if (params['defaultSubnets'] is String) {
      newDefaultSubnets = jsonParser.parse(newDefaultSubnets).value;
    }

    // (array) defaultPorts
    lNode.attributes['@defaultPorts'] = newDefaultPorts;
    lNode.listChangeController.add('@defaultPorts');

    // (array) defaultSubnets
    lNode.attributes['@defaultSubnets'] = newDefaultSubnets;
    lNode.listChangeController.add('@defaultSubnets');

    // (string) Device Management Endpoint
    lNode.attributes['@defaultDeviceManager'] = params['defaultDeviceManager'];
    lNode.listChangeController.add('@defaultDeviceManager');

    link.save();

    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };
}

/// [DONE] will change credentials to interact with device manager for all new children
Future<Map<String,dynamic>> changeCredentials(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  for (String key in params.keys) {

    lNode.attributes[r'$$' + '$key'] = params['$key'];
    lNode.listChangeController.add(r'$$' + '$key');
  }

  try {
    link.save();

    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };
}

Future<Map<String, dynamic>> testHttp(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  String urlEndpoint = params['targetUri'];
  var headerIn = params['headers'];
  String body = params['body'];

  Uri postUrl = Uri.parse(urlEndpoint);
  Map<String, dynamic> callHeaders = helper.mapParser(headerIn);

  http.Response response = await http.post(postUrl, headers: callHeaders, body: body);

  succeeded = true;

  return {
    'succeeded' : succeeded,
    'statusCode' : response.statusCode
  };

}

/// [DONE] will change the posting rate at root, ie time between webjob events
Future<Map<String, dynamic>> updatePostingRate(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  double postingRate = double.parse(params['postRateInMin']);

  lNode.attributes['@postRateInMin'] = postingRate;
  lNode.listChangeController.add('@postRateInMin');

  try {

    globals.timer.cancel();

    var newTimerDuration = new Duration(seconds: (postingRate*60).floor());
    globals.timer = new Timer.periodic(newTimerDuration, (_) async {
      await globals.updateFunction(link);
    });

    link.save();
    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };


}

/// [DONE] will change the length of the SaS token expiry
Future<Map<String, dynamic>> updateSasTokenExpiration(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  int postingRate = int.parse(params['sasTokenExpirationInMin'].toString());

  lNode.attributes['@sasTokenExpirationInMin'] = postingRate;
  lNode.listChangeController.add('@sasTokenExpirationInMin');

  link.save();
  succeeded = true;

  return {
    'succeeded' : succeeded
  };


}

/// [DONE] will change the batch size for searching networks
Future<Map<String, dynamic>> updateIpBatchSearchSize(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  lNode.attributes['@ipBatchSearchSize'] = params['ipBatchSearchSize'];
  lNode.listChangeController.add('@ipBatchSearchSize');

  try {

    link.save();
    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };


}

/// [DONE] will change ip range to search against by default, allows for subnet wildcard
Future<Map<String, dynamic>> changeRecoveryIpRange(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  LocalNode lNode = link.getNode(pathOfParent);

  String startIp = params['startIp'];
  String endIp = params['endIp'];

  if (!verify.validIpRange(startIp, endIp)) {
    return {
      'succeeded' : succeeded
    };
  }

  try {

    lNode.attributes['@recoveryIpRange'] = [startIp, endIp];
    lNode.listChangeController.add('@recoveryIpRange');

    link.save();
    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };


}

///////////////////////////
// WeMo Node Profiles!
///////////////////////////

/// [DONE] call into wemo device to sync to specified timezone
Future<Map<String,dynamic>> setTimeZone(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool response = false;

  /// TODO:[Day3] [TimeSync] change the below to proper class
//  bool error = false;
//
//  int utcTime = double.parse(params['epochTime']).round();
//  var timezoneOffset = params['timezoneOffset'];
//  int dst = params['dst'] ? 1 : 0;
//  int dstSupported = params['dstSupported'] ? 1 : 0;
//
//  LocalNode lNode = link.getNode(pathOfParent);
//
//  String ipAddress = lNode.get('@ipAddress');
//  String port = lNode.get('@port');



  response = true;

  return {
    'succeeded' : response
  };

}

/// [DONE] call into wemo device to change friendly name
Future<Map<String,dynamic>> changeFriendlyName(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  String newFriendlyName = params['newFriendName'].toString();

  LocalNode lNode = link.getNode(pathOfParent);
  LocalNode lRootNode = link.getNode('/');

  String ipAddress = lNode.get('@ipAddress');
  String port = lNode.get('@port');

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  wemo_calls.ChangeFriendlyNameCall changeFriendlyNameCall =
      new wemo_calls.ChangeFriendlyNameCall(ipAddress, port, defaultPorts);

  await changeFriendlyNameCall.callWemo(newFriendlyName);

  if (changeFriendlyNameCall.succeeded) {
    lNode.attributes['@friendlyName'] = newFriendlyName;
    lNode.listChangeController.add('@friendlyName');
    link.save();
  }

  return {
    'succeeded' : changeFriendlyNameCall.succeeded
  };

}

/// [DONE] call into wemo device to get current state
Future<Map<String,dynamic>> getState(String pathOfParent, LinkProvider link) async {

  LocalNode lNode = link.getNode(pathOfParent);
  LocalNode lRootNode = link.getNode('/');

  String ipAddress = lNode.get('@ipAddress');
  String port = lNode.get('@port');

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  wemo_calls.GetStateCall getStateCall =
      new wemo_calls.GetStateCall(ipAddress, port, defaultPorts);

  await getStateCall.callWemo();

  if (getStateCall.succeeded) {
    link.val('$pathOfParent/stateValue', getStateCall.binaryState);
  }

  return {
    'switchState': getStateCall.binaryState,
    'succeeded': getStateCall.succeeded
  };

}

/// [DONE] call into wemo device to get current signal strength
Future<Map<String,dynamic>> getSignalStrength(String pathOfParent, LinkProvider link) async {

  LocalNode lNode = link.getNode(pathOfParent);
  LocalNode lRootNode = link.getNode('/');

  String ipAddress = lNode.get('@ipAddress');
  String port = lNode.get('@port');

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  wemo_calls.GetSignalStrengthCall getSignalStrengthCall =
    new wemo_calls.GetSignalStrengthCall(ipAddress, port, defaultPorts);

  await getSignalStrengthCall.callWemo();

  if (getSignalStrengthCall.succeeded) {
    link.val('$pathOfParent/signalStrength', getSignalStrengthCall.signalStrength);
  }

  return {
  'signalStrength'  : getSignalStrengthCall.signalStrength,
  'succeeded'       : getSignalStrengthCall.succeeded
  };

}

/// [###] call into wemo to get current full firmware version string
Future<Map<String,dynamic>> getFirmwareVersion(String pathOfParent, LinkProvider link) async {

  LocalNode lNode = link.getNode(pathOfParent);
  LocalNode lRootNode = link.getNode('/');

  String ipAddress = lNode.get('@ipAddress');
  String port = lNode.get('@port');

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  wemo_calls.GetFirmwareVersionCall getFirmwareVersionCall =
  new wemo_calls.GetFirmwareVersionCall(ipAddress, port, defaultPorts);

  await getFirmwareVersionCall.callWemo();

  return {
  'firmwareVersion' : getFirmwareVersionCall.firmwareVersion,
  'succeeded'       : getFirmwareVersionCall.succeeded
  };

}

/// [###] call into wemo to upgrade firmware
Future<Map<String,dynamic>> firmwareUpdate(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  ///TODO:[Day3] need to finalize logic for firmware update, should pause all until firmware update goes through


  return {
    'succeeded'       : true
  };

}

/// [###] call into wemo to push schedule to wemo / update node value [Schedule, OverrideTime]
Future<Map<String,dynamic>> storeSchedule(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  LocalNode lNode = link.getNode(pathOfParent);
  LocalNode lRootNode = link.getNode('/');

  String base64db = params['base64db'];

  String ipAddress = lNode.get('@ipAddress');
  String port = lNode.get('@port');

  String nowIsoString = new DateTime.now().toIso8601String();

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  //// get dbVersion through call, will verify reliable connectivity
  wemo_calls.GetRulesInfoCall getRulesInfoCall =
      new wemo_calls.GetRulesInfoCall(ipAddress, port, defaultPorts);

  int dbVersion = await getRulesInfoCall.callWemo();

  if (getRulesInfoCall.succeeded) {

  } else {

    // TODO:[Day2] process error handling

  }

  //// now i will be pushing actual data to lightswitch
  String ruleBody = "&lt;![CDATA[$base64db]]&gt;";

  wemo_calls.StoreRulesCall storeRulesCall =
      new wemo_calls.StoreRulesCall(ipAddress, port, defaultPorts);

  await storeRulesCall.callWemo(ruleBody, (dbVersion+1).toString(), '1');

  lNode.attributes['@lastDBPush'] = nowIsoString;
  lNode.listChangeController.add('@lastDBPush');

  lNode.attributes['@dbVersion'] = dbVersion+1;
  lNode.listChangeController.add('@dbVersion');

  link.save();

  if (storeRulesCall.succeeded) {

    await updateSchedule(pathOfParent, params, link).catchError( (e)
    {
      storeRulesCall.succeeded == false;
    });

  }

  return {
    'succeeded' : storeRulesCall.succeeded
  };

}

/// [DONE] call into DSA to update current node values [schedule, overridetime]
Future<Map<String,dynamic>> updateSchedule(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool response = false;

  var scheduleIn = params['newSchedule'];
  int overrideTime = int.parse(params['overrideTime'].toString());

  Map<String,dynamic> schedule = helper.mapParser(scheduleIn);

  link.val('$pathOfParent/overrideTime', overrideTime);
  link.val('$pathOfParent/schedule', schedule);

  if (link.val('$pathOfParent/overrideTime') == overrideTime && link.val('$pathOfParent/schedule') == schedule) {
    response = true;
  }

  link.saveAsync();

  return {
    'succeeded' : response
  };

}

/// [###] call into DSA to update current node values [customEvents] and store shcedule (to EMcore) to push new schedule
Future<Map<String,dynamic>> storeCustomEvents(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool response = false;
//  Map<String,dynamic> weeklyScheduleIn = link.val('$pathOfParent/schedule');
//  Map<String,dynamic> currentCustomEvents = link.val('$pathOfParent/customEvents');

  var customEventsIn = params['customEvents'];

  Map<String, dynamic> customEvents = {};

  customEvents = helper.mapParser(customEventsIn);

  List<events.InitialCustomEvent> eventsList = [];

  customEvents.forEach((String key, dynamic value) =>
    eventsList.add(new events.InitialCustomEvent(key, value))
  );


  // this will replace the events if the issue if no issues
  link.updateValue('${pathOfParent}/customEvents',customEvents);

  /// TODO:[Day3] need to check against or completely replace existing vacations
  /// TODO:[Day2] need to add method to interact with emcore if new schedule is needed

  link.saveAsync();
  response = true;

  return{
    'succeeded' : response
  };
}

/// [DONE] toggle subscription to get updates from wemo device
Future<Map<String,dynamic>> toggleSubscription(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool response = false;

  LocalNode lNode = link.getNode(pathOfParent);
  bool currentStateSubscription = (lNode.get('@subscription').toString().toLowerCase() == 'true');

  lNode.attributes['@subscription'] = !currentStateSubscription;
  lNode.listChangeController.add('@subscription');

  try {

    link.save();
    response = true;

  } catch (e) {

    return {
      'succeeded': response
    };

  }

  /// TODO:[Day3] re introduce proper subscription for CoV

  return {
    'succeeded' : response
  };

}

/// [DONE] toggle posting, this attribute decides if data is pushed
Future<Map<String,dynamic>> togglePosting(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool response = false;

  LocalNode lNode = link.getNode(pathOfParent);
  bool currentStateSubscription = (lNode.get('@posting').toString().toLowerCase() == 'true');

  lNode.attributes['@posting'] = !currentStateSubscription;
  lNode.listChangeController.add('@posting');

  try {

    link.save();
    response = true;

  } catch (e) {

    return {
      'succeeded': response
    };

  }

  return {
    'succeeded' : response
  };

}

/// [###] add publisher settings where date is being pushed
Future<Map<String,dynamic>> publisherSettings(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  LocalNode lNode = link.getNode(pathOfParent);

  params.forEach((attributeName,newStringValue) {

    params[attributeName].toString().toLowerCase();
    lNode.attributes['@$attributeName'] = newStringValue;
    lNode.listChangeController.add('@$attributeName');

  });

  lNode.attributes['@posting'] = true;
  lNode.listChangeController.add('@posting');

  ///TODO:[Day3] [Publisher Settings] need to maek sure to push once to initialize data

  bool updateState = true;
//  await wemo_ls_node.updateNodePointValues(link, lNode.getAttribute('@uuid'));
  bool postState = true;
//  await wemo_ls_node.httpUpdate(link, lNode.getAttribute('@uuid'));

  link.save();

  bool response = (updateState || postState) ? true : false;

  return{
  'succeeded' : response
  };

}

///////////////////////////
// Cron job has to go somewhere accessible
///////////////////////////