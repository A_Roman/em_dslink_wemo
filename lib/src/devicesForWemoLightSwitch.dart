import 'package:em_dslink_wemo/src/globals.dart' as globals;

import "package:upnp/upnp.dart";
import "dart:async";

/// Simple Get Device using specified ip and port
Future<Device> getDevice(String ip, String port) async {

  DiscoveredClient discoveredClient = new DiscoveredClient();
  discoveredClient.location = "http://${ip}:${port}/setup.xml";

  Device deviceAttempt = null;
  await discoveredClient.getDevice().then( (Device device) {
    deviceAttempt = device;
  }).catchError( (error) {

  });

  return deviceAttempt;
}

Future<Service> getSpecificService(Device device, String serviceName ) async {

  return await device.getService('urn:Belkin:serviceId:${serviceName.toLowerCase()}1');

}

/// Get device from an IP and a list of acceptable ports
Future<Device> searchForDeviceInIP(String ipAddress, List<String> portsList, {String expectedPort = null}) async {

  Device tempDevice = null;

  if (expectedPort != null){
    tempDevice = await getDevice(ipAddress, expectedPort);
    if (tempDevice == null) {
      portsList.remove(expectedPort);
    } else {
      return tempDevice;
    };
  }

  List<Future<Device>> listOfFutures = [];

  for (String port in portsList) {
    listOfFutures.add(getDevice(ipAddress, port));
  }

  List<Device> searchResult = await Future.wait(listOfFutures);
  searchResult.forEach( (device) {
    if (device != null) {
      tempDevice = device;
    }
  });

  return tempDevice;
}

/// Get a list of device in a range of IPs
Future<List<Device>> searchForDevicesInList(List<String> listOfIps, List<String> listOfPorts, int ipBatchSearchSize) async {

  List<Device> listOfDevices = [];
  List<Future<Device>> listOfFutures = [];

  // one huge list of all IP futures
  for (String singleIp in listOfIps) {
      listOfFutures.add(searchForDeviceInIP(singleIp, listOfPorts));
  }

  globals.debuggingTool('I will be searching for ${listOfFutures.length.toString()} in batches of $ipBatchSearchSize');

  List<List<Future<Device>>> listOfFutureBatches = listToBatches(listOfFutures, ipBatchSearchSize);

//  int i = 0;
  for (List<Future<Device>> batchOfFutures in listOfFutureBatches) {

    // print('batch # $i contains ${batchOfFutures.length} futures');
//    int j = 0;

    await Future.wait(batchOfFutures).then( (List<Device>listOfResponse) {
      for (Device foundDevice in listOfResponse) {
        if (foundDevice != null) {
          listOfDevices.add(foundDevice);
//          j++;
        }
      }

    });


    // print('  found ${j} switches');

//    i++;

  }

  return listOfDevices;
}

Future<Map<String, Device>> getDevicesList() async {

  DeviceDiscoverer discoverer = new DeviceDiscoverer();

  Map<String, Device> foundLightSwitches = {};

  try {
    await for (DiscoveredClient client in discoverer.quickDiscoverClients(
        timeout: const Duration(seconds: 10), searchInterval: const Duration(seconds: 30)
    )) {

      String nameSection = client.usn.split(':')[1].split('-')[0];
      Device deviceFound;

      if (nameSection == "Lightswitch") {

        try {
          deviceFound = await client.getDevice();
        } catch (e) {
          continue;
        }

        foundLightSwitches[deviceFound.uuid] = deviceFound;

      }
      continue;

    }
  } catch (e) {

  }

  discoverer.stop();

  return foundLightSwitches;
}

List<List<dynamic>> listToBatches(List<dynamic> originalList, int batchSize) {

  List<List<dynamic>> listOfBatches = [];

  int numberOfBatches = (originalList.length / batchSize).floor();
  if ((originalList.length % batchSize) != 0) {
    numberOfBatches++;
  }

  // now split into batches
  int i = 0;
  while(i < numberOfBatches) {

    List<String> batch = [];

    if (originalList.length > batchSize) {

      batch = originalList.sublist(0, batchSize);
      originalList.removeRange(0, batchSize);

    } else {

      batch = originalList;

    }

    listOfBatches.add(batch);
    i++;
  }

  return listOfBatches;
}