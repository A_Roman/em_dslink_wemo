import "dart:async";
import 'package:upnp/upnp.dart';

Future<Device> _getDevice(String ip, String port) async {

  DiscoveredClient discoveredClient = new DiscoveredClient();
  discoveredClient.location = "http://${ip}:${port}/setup.xml";

  Device deviceAttempt = await discoveredClient.getDevice();

  return deviceAttempt;
}

Future<Service> _getSpecificService(Device device, String serviceName ) async {

  return await device.getService('urn:Belkin:serviceId:${serviceName.toLowerCase()}1');

}

/////////////////////////////////////////////////////////////
//// Now Creating Classes for better structure of responses
/////////////////////////////////////////////////////////////

//abstract class wemoBaseCall {
//
//  String ipAddress;
//  String port;
//  List<String> extendedPortsList;
//
//  bool succeeded = false;
//
//  wemoBaseCall(this.ipAddress, )
//
//
//}

////////////////////////
// BASIC EVENT
////////////////////////

class GetStateCall  {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  int binaryState;
  bool succeeded = false;

  GetStateCall(String ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<int> callWemo() async {

    await _getStateUpnpCall(ipAddress, port).then((Map<String,dynamic> response) {
      binaryState = int.parse(response['BinaryState'].toString());
      succeeded = true;
    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return binaryState;
  }

  Future<Map<String,dynamic>> _getStateUpnpCall(String ipAddress, String port) async {

    Map<String,dynamic> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'basicevent');

//|---- BinaryState  is related to state value >BinaryState< has direction {OUT}
    response = await service.invokeAction('GetBinaryState', {});

    return response;
  }

}

class GetSignalStrengthCall {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  int signalStrength;
  bool succeeded = false;

  GetSignalStrengthCall(String ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<int> callWemo() async {

    await _getSignalStrength(ipAddress, port).then((Map<String,dynamic> response) {
      signalStrength = int.parse(response['SignalStrength'].toString());
      succeeded = true;
    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return signalStrength;
  }

  Future<Map<String,dynamic>> _getSignalStrength(String ipAddress, String port) async {

    Map<String,dynamic> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'basicevent');

//|---- SignalStrength  is related to state value >SignalStrength< has direction {OUT}
    response = await service.invokeAction('GetSignalStrength', {});

    return response;
  }

}

class ChangeFriendlyNameCall {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  String newFriendlyName;

  bool succeeded = false;

  ChangeFriendlyNameCall(String ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<String> callWemo(String newFriendlyNameIn) async {

    await _changeFriendlyName(newFriendlyNameIn).then((Map<String,dynamic> response) {
      newFriendlyName = response['FriendlyName'];
      succeeded = true;
    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return newFriendlyName;
  }

  Future<dynamic> _changeFriendlyName(String newFriendlyName) async {

    Map<String, dynamic> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'basicevent');

//|---- FriendlyName  is related to state value >FriendlyName< has direction {IN}

    response = await service.invokeAction('ChangeFriendlyName', {
      'FriendlyName' : newFriendlyName
    });

    return response;

  }


}

/////////////////////
// FIRMWARE EVENT
/////////////////////

class GetFirmwareVersionCall {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  String firmwareVersion;
  String skuNo;
  double firmwareVersionDouble;

  bool succeeded = false;

  GetFirmwareVersionCall (ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<String> callWemo() async {

    await _getFirmwareVersion().then((Map<String,dynamic> response) {

      List<String> splitResponse = response["FirmwareVersion"].toString().split("|");

      firmwareVersion = splitResponse.first.split(":").last;
      skuNo = splitResponse.last.split(":").last;

      succeeded = true;

    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return firmwareVersion;
  }

  double getFirmwareDouble() {

    try {

      List<String> splitVersion = firmwareVersion.split("_").last.split(".");

      List<int> approvedSection = [];
      List<int> decimalCount = [];

      splitVersion.forEach((String subSection) {
        int tempInterger = int.parse(subSection, onError: (s) => null);
        if (tempInterger != null) {
          approvedSection.add(tempInterger);
          decimalCount.add(tempInterger.toString().length);
        }
      });

      decimalCount.removeLast();

      double acceptableVersionNumber = double.parse("${approvedSection.join("")}.${decimalCount.join("")}");

      firmwareVersionDouble = acceptableVersionNumber;

    } catch (e) {

      succeeded = false;

    }

    return firmwareVersionDouble;
  }

  Future<dynamic> _getFirmwareVersion() async {
    Map<String,String> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'firmwareupdate');

//|---- ruleDbPath  is related to state value >ruleDbPath< has direction {OUT}
//|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {OUT}
//|---- errorInfo  is related to state value >errorInfo< has direction {OUT}

    response = await service.invokeAction('GetFirmwareVersion', {});

    return response;
  }

}


///////////////////////
// RULES
///////////////////////

class GetRulesInfoCall {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  String ruleDbPath;
  int ruleDbVersion = null;
  String errorInfo;
  bool succeeded = false;

  GetRulesInfoCall (ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<int> callWemo() async {

    await _getRulesInfo().then((Map<String,dynamic> response) {
      ruleDbPath = response['ruleDbPath'];
      ruleDbVersion = int.parse(response['ruleDbVersion'].toString());
      errorInfo = response['errorInfo'];

      (errorInfo == 'SUCCESS') ? succeeded = true : throw('Error - LightSwitch rejected schedule');

    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return ruleDbVersion;
  }

  Future<dynamic> _getRulesInfo() async {
    Map<String,String> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'rules');

//|---- ruleDbPath  is related to state value >ruleDbPath< has direction {OUT}
//|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {OUT}
//|---- errorInfo  is related to state value >errorInfo< has direction {OUT}

    response = await service.invokeAction('FetchRules', {});

    return response;
  }

}

class StoreRulesCall {

  String ipAddress;
  String port;
  List<String> extendedPortsList;

  bool succeeded = false;

  StoreRulesCall (ipAddressIn, String portIn, List<String> listOfPortsIn) {

    ipAddress = ipAddressIn;
    port = portIn;
    extendedPortsList = listOfPortsIn;

  }

  Future<bool> callWemo(String ruleBody, String dbVersion, String proccesDB) async {

    await _storeRules(ruleBody, dbVersion, proccesDB).then((Map<String,dynamic> response) {
      (response.values.first == "Storing of rules DB Successful!\n") ? succeeded = true : succeeded = false;
    }).catchError( (errorFromCall) {
      succeeded = false;
    });

    return succeeded;
  }

  Future<dynamic> _storeRules(String ruleBody, String dbVersion, String proccesDB) async {

    Map<String,String> response;

    Device device = await _getDevice(ipAddress, port);
    Service service = await _getSpecificService(device, 'rules');

//|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {IN}
//|---- processDb  is related to state value >processDb< has direction {IN}
//|---- ruleDbBody  is related to state value >ruleDbBody< has direction {IN}
//|---- errorInfo  is related to state value >errorInfo< has direction {OUT}

    response = await service.invokeAction('StoreRules',
        {
          'ruleDbVersion' : dbVersion,
          'processDb'     : proccesDB,
          'ruleDbBody'    : ruleBody
        }
    );

    return response;

  }

}
