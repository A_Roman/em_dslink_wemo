import 'package:dslink/dslink.dart';
import 'package:upnp/upnp.dart';
import 'package:em_dslink_wemo/src/wemoCallsMapped.dart' as wemo_calls;
import 'package:em_dslink_wemo/src/ioTHubIntegration.dart' as iot;
import 'dart:async';

class WeMoNode extends SimpleNode {

  //// SET ON INITIALIZATION
  // acquired from initialization
  String nodeRootPath;
  String uuid;
  String ipAddress;
  String port;

  // inherited from root
  List<String> listOfPorts;
  String deviceManager;           // stuff needed for the vacations "schedule"
  String deviceManagerUsername;   // stuff needed for the vacations "schedule"
  String deviceManagerPassword;   // stuff needed for the vacations "schedule"

  //// UNIQUE INFORMATION
  // device information
  String friendlyName;
  String mac; // TODO:: need to add MAC address support, +1 to the hex Mac on getExtMeta
  // triggers for events
  bool subscription = true;
  bool posting = false;

  // stuff needed for data into EM.Core
  String userKey ;
  String deviceKey;
  String iotDeviceId;
  String iotHubUri;
  String iotDeviceToken;

  //// place holder to transfer data
  Map<String, dynamic> nodeMap;

  /// Constructor requires a string to build path for class, optional parameters are
  /// the device or the ipAddressAndPort separated by colon
  WeMoNode(this.uuid,
      {Device deviceIn = null, String ipAddressAndPortIn}
      ) : super('/$uuid') {

    nodeRootPath = '/$uuid';

    // this will add a new device to the tree
    if (deviceIn != null) {

      List<String> brokenAddressFromDevice = deviceIn.urlBase.split('/')[2].split(':');
      ipAddress = brokenAddressFromDevice[0];
      port = brokenAddressFromDevice[1];
      friendlyName = deviceIn.friendlyName;

      nodeMap = _buildTree();

    // this will be get class from node triggered on reboot/reconnection
    } else if (ipAddressAndPortIn != null) {

      List<String> brokenAddress = ipAddressAndPortIn.split(':');
      ipAddress = brokenAddress.first;
      port = brokenAddress.last;
      friendlyName = '';

    } else {
      throw("missing atleast one identified; either device or ipAddressAndPortIn");
    }

  }

//  Future<Map<String,dynamic>> update() async {
//
//    mac = '0000000000';
//
////    if (rootNode.exists) {
////
////      // From Root node
////      deviceManager = rootNode.getAttribute('@defaultDeviceManager');
////
////    } else {
////
////
////
////    }
//
//    nodeMap = _buildTree();
//
//  }

  ///TODO:[Day3] need to implement the folloowign to pull all relevant data from the devices
//  Map<String, dynamic> _getDeviceInfo() {
//
//  }

  Map<String, dynamic> _buildTree() {

    Map<String, dynamic> wemoNodeMap = {};

    Map<String, dynamic> wemoLSConfigs = {
      r'$name': uuid,
      r'$is': 'deviceRoot',

      r'$DMUser': '',
      r'$$DMPassword': '',
    };

    wemoNodeMap.addAll(wemoLSConfigs);

    Map<String, dynamic> wemoLSAttributes = {
      '@firmwareVer'      : 0,
      '@uuid'             : uuid,
      '@mac'              : mac,

      '@ipAddress'        : ipAddress,
      '@port'             : port,

      '@initialConn'      : new DateTime.now().toUtc().toIso8601String(),
      '@friendlyName'     : friendlyName,
      '@dbVersion'        : 0,
      '@lastDBPush'       : 'never',
      '@subscription'     : subscription,
      '@posting'          : posting,

      '@userKey'          : userKey,
      '@deviceKey'        : deviceKey,
      '@iotHubUri'        : iotHubUri,
      '@iotHubDeviceId'   : iotDeviceId,
      '@iotHubDeviceToken': iotDeviceToken,

      '@DeviceManager'    : ''
    };

    wemoNodeMap.addAll(wemoLSAttributes);

    ///////////
    //ACTIONS//
    ///////////

    Map<String, dynamic> timeSyncNode = { 'setTimeZone': {
      r'$name': 'Set Time Zone',
      r'$invokable': 'write',

      r'$columns': [
        {
          'name': 'succeeded',
          'type': 'bool'
        },
      ],

      r'$results': 'values',
      r'$params': [
        {
          'name': 'epochTime',
          'type': 'string'
        },
        {
          'name': 'timezoneOffset',
          'type': 'number'
        },
        {
          'name': 'dst',
          'type': 'bool'
        },
        {
          'name': 'dstSupported',
          'type': 'bool'
        }
      ],

      r'$is': 'setTimeZone'
    }};

    wemoNodeMap.addAll(timeSyncNode);

    Map<String, dynamic> changeFriendlyNameNode = { 'changeFriendlyName': {
      r'$name': 'Change Friendly Name',
      r'$invokable': 'write',

      r'$columns': [
        {
          'name': 'succeeded',
          'type': 'bool'
        }
      ],

      r'$results': 'values',
      r'$params': [
        {
          'name': 'newFriendName',
          'type': 'string'
        }
      ],

      r'$is': 'changeFriendlyName'
    }};

    wemoNodeMap.addAll(changeFriendlyNameNode);

    Map<String,dynamic> getStateNode = { 'getState' : {
      r'$name' : 'Get Switch State',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'switchState',
          'type' : 'string'
        },
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'getState'
    }};

    wemoNodeMap.addAll(getStateNode);

    Map<String,dynamic> getSignalStrengthNode = { 'getSignalStrength' : {
      r'$name' : 'Get Signal Strength',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'signalStrength',
          'type' : 'number'
        },
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'getSignalStrength'
    }};

    wemoNodeMap.addAll(getSignalStrengthNode);

    Map<String,dynamic> getFirmwareVersionNode = { 'getFirmwareVersion' : {
      r'$name' : 'Get Firmware Version',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'lightswitchFirmwareVersion',
          'type' : 'string'
        },
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'getFirmwareVersion'
    }};

    wemoNodeMap.addAll(getFirmwareVersionNode);

    Map<String, dynamic> firmwareUpdateNode = { 'firmwareUpdate' : {
      r'$name' : 'Update Firmware',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name': 'newFirmwareVersion',
          'type': 'string'
        }
        ,{
          'name': 'releaseData',
          'type': 'string'
        }
        ,{
          'name': 'URL',
          'type': 'string'
        }
        ,{
          'name': 'signature',
          'type': 'string'
        }
      ],

      r'$results' : 'values',
      r'$columns' : [
        {
          'name': 'deviceResponse',
          'type': 'string'
        }
      ],

      r'$is' : 'firmwareUpdate'
    }};

    wemoNodeMap.addAll(firmwareUpdateNode);

    Map<String,dynamic> storeScheduleNode = { 'storeSchedule' : {
      r'$name' : 'Store Schedule',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name': 'newSchedule',
          'type': 'map'
        }
        ,{
          'name': 'overrideTime',
          'type': 'number'
        }
        ,{
          'name': 'base64db',
          'type': 'string'
        }
      ],

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'storeSchedule'
    }};

    wemoNodeMap.addAll(storeScheduleNode);

    Map<String, dynamic> updateScheduleNode = { 'updateSchedule' : {
      r'$name' : 'Update Schedule',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name': 'newSchedule',
          'type': 'map'
        }
        ,{
          'name': 'overrideTime',
          'type': 'number'
        }
      ],

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'updateSchedule'
    }};

    wemoNodeMap.addAll(updateScheduleNode);

    Map<String,dynamic> storeCustomEventsNode = { 'storeCustomEvents' : {
      r'$name' : 'Store Custom Events',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name': 'customEvents',
          'type': 'map'
        }
      ],

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'storeCustomEvents'
    }};

    wemoNodeMap.addAll(storeCustomEventsNode);

    Map<String, dynamic> toggleSubscriptionNode = { 'toggleSubscription' : {
      r'$name' : 'Toggle Subscription',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'toggleSubscription'

    }};

    wemoNodeMap.addAll(toggleSubscriptionNode);

    Map<String,dynamic> togglePostingNode = { 'togglePosting' : {
      r'$name' : 'Toggle Posting',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'togglePosting'

    }};

    wemoNodeMap.addAll(togglePostingNode);

    Map<String, dynamic> publisherSettingsNode = { 'publisherSettings' : {
      r'$name' : 'Add Publisher Settings',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name' : 'userKey',
          'type' : 'string'
        },{
          'name' : 'deviceKey',
          'type' : 'string'
        },{
          'name' : 'iotHubUri',
          'type' : 'string'
        },{
          'name' : 'iotHubDeviceId',
          'type' : 'string'
        },{
          'name' : 'iotHubDeviceToken',
          'type' : 'string'
        }
      ],

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'publisherSettings',
    }};

    wemoNodeMap.addAll(publisherSettingsNode);

    Map<String,dynamic> credentialsNode = { 'DMCredentials' : {
      r'$name' : 'Change Device Manager Credencials',
      r'$invokable' : 'write',

      r'$params'      : [
        {
          'name'    : 'DMUser',
          'type'    : 'string',
        },
        {
          'name'    : 'DMPassword',
          'type'    : 'string',
          'editor'  : 'password',
        }
      ],

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'remove'
    }};

    wemoNodeMap.addAll(credentialsNode);

    Map<String,dynamic> removeNode = { 'remove' : {
      r'$name' : 'Remove Device',
      r'$invokable' : 'write',

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'remove'
    }};

    wemoNodeMap.addAll(removeNode);

    //////////
    //VALUES//
    //////////

    Map<String,dynamic> scheduleNode = { 'schedule' : {
      r'$name' : 'Schedule',
      r'$type' : 'map',
      '?value' : {
        "mon":[],
        "tue":[],
        "wed":[],
        "thu":[],
        "fri":[],
        "sat":[],
        "sun":[]
      },
      r'$is' : 'schedule',
    }};

    wemoNodeMap.addAll(scheduleNode);

    Map<String,dynamic> thisWeekScheduleNode = { 'thisWeekSchedule' : {
      r'$name' : 'This Week Schedule',
      r'$type' : 'map',
      '?value' : {
        "mon":[],
        "tue":[],
        "wed":[],
        "thu":[],
        "fri":[],
        "sat":[],
        "sun":[]
      },
      r'$is' : 'thisWeekSchedule',
    }};

    wemoNodeMap.addAll(thisWeekScheduleNode);

    Map<String,dynamic> customEventsNode = { 'customEvents' : {
      r'$name' : 'Custom Events',
      r'$type' : 'map',
      '?value' : { },
      r'$is' : 'customEvents',
    }};

    wemoNodeMap.addAll(customEventsNode);


    Map<String,dynamic> stateValueNode = {'stateValue' : {
      r'$name' : 'Switch State',
      r'$type' : 'number',
      '?value' : 0,
      r'$is' : 'switchState',
    }};

    wemoNodeMap.addAll(stateValueNode);

    Map<String,dynamic> lightswitchFirmwareVersionNode = { 'lightswitchFirmwareVersion' : {
      r'$name' : 'Firmware Version',
      r'$type' : 'number',
      '?value' : 0, // versionNumber,
      r'$is' : 'lightswitchFirmwareVersion',
    }};

    wemoNodeMap.addAll(lightswitchFirmwareVersionNode);

    Map<String,dynamic> signalStrengthNode = { 'signalStrength' : {
      r'$name' : 'Signal Strength',
      r'$type' : 'number',
      '?value' : 0,
      r'$is' : 'signalStrength',
    }};

    wemoNodeMap.addAll(signalStrengthNode);

    Map<String,dynamic> deviceConnectionStatusNode = { 'deviceConnectionStatus' : {
      r'$name' : 'Device Connection Status',
      r'$type' : 'number',
      '?value' : 0,
      r'$is' : 'deviceConnectionStatus',
    }};

    wemoNodeMap.addAll(deviceConnectionStatusNode);

    Map<String,dynamic> overrideTimeNode = { 'overrideTime' : {
      r'$name' : 'Override Time',
      r'$type' : 'number',
      '?value' : 0,
      r'$is' : 'overrideTime',
    }};

    wemoNodeMap.addAll(overrideTimeNode);

    return wemoNodeMap;
  }

  addNodeToTree(LinkProvider link) async {

    link.addNode('/$uuid', this.nodeMap);
    link.save();

  }

  Future<bool> updatePointValues(LinkProvider link) async {

    bool success = await updateNodePointValues(link, uuid);

    return success;

  }

//  Future<bool> httpUpdate(LinkProvider link) async {
//
//    LocalNode lNode = link.getNode('/$uuid');
//    LocalNode rootNode = link.getNode('/');
//
//    String iotHubUri = lNode.getAttribute('@iotHubUri');
//    String iotDeviceId = lNode.getAttribute('@iotHubDeviceId');
//    String iotDeviceToken = lNode.getAttribute('@iotHubDeviceToken');
//    int minsToExpiration = int.parse(rootNode.get('@sasTokenExpirationInMin').toString());
//
//    String userKey = lNode.getAttribute('@userKey');
//    String deviceKey = lNode.getAttribute('@deviceKey');
//
//    String connectorName = "DSA-WeMo";
//
//    int stateValue = link.val('/$uuid/stateValue');
//    int signalStrength = link.val('/$uuid/signalStrength');
//    int deviceConnectionStatus = link.val('/$uuid/deviceConnectionStatus');
//
//    Map<String,dynamic> points = {
//      'stateValue': stateValue,
//      'signalStrength': signalStrength,
//      'deviceConnectionStatus': deviceConnectionStatus
//    };
//
//    String sas = iot.generateSaSToken(iotHubUri, iotDeviceToken, iotDeviceId, minsToExpiration);
//    Map<String, dynamic> payload = iot.buildPayload(iotDeviceId, userKey, deviceKey, connectorName, points);
//    int statusCode = await iot.httpPost(sas, iotHubUri, payload);
//
//    bool response = (statusCode == 204) ? true : false;
//    // print(statusCode);
//
//
//    return response;
//
//  }

}

Future<bool> updateNodePointValues(LinkProvider link, String uuid) async {

  int deviceConnectionStatus = 1;
  bool response = true;

  LocalNode lNode = link.getNode('/$uuid');
  LocalNode lRootNode = link.getNode('/');

  String ipAddress = lNode.getAttribute('@ipAddress');
  String port = lNode.getAttribute('@port');

  List<String> defaultPorts = lRootNode.get('@defaultPorts');

  // get state val
  wemo_calls.GetStateCall getStateCall =
  new wemo_calls.GetStateCall(ipAddress, port, defaultPorts);

  await getStateCall.callWemo();

  if (getStateCall.succeeded) {
    link.val('/$uuid/stateValue', getStateCall.binaryState);
  }

  // get signal strength
  wemo_calls.GetSignalStrengthCall getSignalStrengthCall =
  new wemo_calls.GetSignalStrengthCall(ipAddress, port, defaultPorts);

  await getSignalStrengthCall.callWemo();

  if (getSignalStrengthCall.succeeded) {
    link.val('/$uuid/signalStrength', getSignalStrengthCall.signalStrength);
  }

  // get firmware version
  wemo_calls.GetFirmwareVersionCall getFirmwareVersionCall =
  new wemo_calls.GetFirmwareVersionCall(ipAddress, port, defaultPorts);

  await getFirmwareVersionCall.callWemo();
  getFirmwareVersionCall.getFirmwareDouble();

  if (getFirmwareVersionCall.succeeded) {
    link.val('/$uuid/lightswitchFirmwareVersion', getFirmwareVersionCall.firmwareVersionDouble);
  }

  // if both calls failed then device connection status is 0 and we were unable to connec to device
  if (!getStateCall.succeeded && !getSignalStrengthCall.succeeded && !getFirmwareVersionCall.succeeded){
    deviceConnectionStatus = 0;
    response = false;
  }

  link.val('/$uuid/deviceConnectionStatus', deviceConnectionStatus);

  return response;

}

Future<bool> httpUpdate(LinkProvider link, String uuid) async {

  LocalNode lNode = link.getNode('/$uuid');
  LocalNode rootNode = link.getNode('/');

  // needed stuff for interaction to IoTHub
  String iotHubUri = lNode.getAttribute('@iotHubUri');
  String iotDeviceId = lNode.getAttribute('@iotHubDeviceId');
  String iotDeviceToken = lNode.getAttribute('@iotHubDeviceToken');
  int minsToExpiration = int.parse(rootNode.get('@sasTokenExpirationInMin').toString());

  // needed for data to make it to the appropiate asset
  String userKey = lNode.getAttribute('@userKey');
  String deviceKey = lNode.getAttribute('@deviceKey');

  String connectorName = "DSA-WeMo";

  var stateValue = link.val('/$uuid/stateValue');
  var signalStrength = link.val('/$uuid/signalStrength');
  var deviceConnectionStatus = link.val('/$uuid/deviceConnectionStatus');
  var lightswitchFirmwareVersion = link.val('/$uuid/lightswitchFirmwareVersion');

  Map<String,dynamic> points = {
    'stateValue': stateValue,
    'signalStrength': signalStrength,
    'deviceConnectionStatus': deviceConnectionStatus,
    'lightswitchFirmwareVersion': lightswitchFirmwareVersion
  };

  String sas = iot.generateSaSToken(iotHubUri, iotDeviceToken, iotDeviceId, minsToExpiration);
  Map<String, dynamic> payload = iot.buildPayload(iotDeviceId, userKey, deviceKey, connectorName, points);
  int statusCode = await iot.httpPost(sas, iotHubUri, payload);

  bool response = (statusCode == 204) ? true : false;

  return response;

}

//Future<bool> updateRelevantAttributes(LinkProvider link, String uuid) async {
//
//  /// TODO:[day3] addtional analytics for troubleshooting multi source of databases
//
//}