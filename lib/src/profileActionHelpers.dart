import 'dart:convert';
import 'package:petitparser/json.dart' as petit_parser;
import 'package:dslink/dslink.dart';

Map<String, dynamic> mapParser(var variableMap) {

  Map<String,dynamic> mapReturn = {};

  petit_parser.JsonParser jsonPetitParser = new petit_parser.JsonParser();

  if (variableMap is String) {

    var jsonPetitParserResponse = jsonPetitParser.parse(variableMap);

    if (jsonPetitParserResponse.isFailure){

      mapReturn = JSON.decode(variableMap);

    } else {

      mapReturn = jsonPetitParserResponse.value;

    }

  } else if (variableMap is Map) {

    mapReturn = variableMap;

  }

  return mapReturn;

}

List<String> getListOfIps(String startIp, String endIp, List<dynamic> listOfSubnets) {

  List<String> tempListOfIps = [];
  List<String> listOfFinalIps = [];

  int ipStart = int.parse(startIp.split('.').last);
  int ipEnd = int.parse(endIp.split('.').last);

  String tempSubnet = startIp.split('.').sublist(0,3).join('.');

  while (ipStart <= ipEnd){

    tempListOfIps.add( [tempSubnet,ipStart.toString()].join('.') );

    ipStart++;
  }

  // if wildcard is present.... we will replace the x with subnets values...
  if ( startIp.toLowerCase().contains('x') && endIp.toLowerCase().contains('x') ) {

    for (dynamic subnet in listOfSubnets) {

      tempListOfIps.forEach( (String ipWithWildcard) {
        listOfFinalIps.add(ipWithWildcard.toLowerCase().replaceAll('x', subnet.toString()));
      });

    }

    // else just pass the existing list....
  } else {
    listOfFinalIps = tempListOfIps;
  }

  return listOfFinalIps;

}

double getIntFromStringVersion() {



}

List<String> getListOfExistingUuid(LinkProvider link) {

  LocalNode lNode = link.getNode('/');

  List<String> existingUuidList = [];

  lNode.children.values.forEach( (Node childNode) {
    if (childNode.getConfig(r'$is') == 'deviceRoot' ) {
      existingUuidList.add(childNode.getConfig(r'$name'));
    }
  });

  return existingUuidList;

}