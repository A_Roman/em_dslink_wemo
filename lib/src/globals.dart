import 'dart:async';
import 'package:dslink/dslink.dart';
import 'package:em_dslink_wemo/src/wemoLightSwitchNode.dart' as wemo_node;
import 'package:em_dslink_wemo/src/profileActions.dart' as profile_action;
import 'package:em_dslink_wemo/src/globals.dart' as globals;

bool debuggingLog = false;

List<String> recoveryIpRange = [];
List<String> defaultSubnets = [];
List<String> defaultports = [];

double postRateInMins = null;
int ipBatchSearchSize = null;

Timer timer = new Timer.periodic(null, null);

updateFunction(LinkProvider link) async {

  debuggingTool('webjob started at ${new DateTime.now()}');

  Map<String, dynamic> children = link.getNode('/').children;

  Map<String ,SimpleNode> listOfDevice = {};

  children.forEach( (String key, var node) {
    String nodeIs = node.get(r'$is');

    if (nodeIs == 'deviceRoot') {
      listOfDevice.addAll({key : node });
    }
  });

  // if devices exist rebase on scheduled function
  if (listOfDevice.length != 0 && link.getNode('/').getAttribute('@timedDiscovery').toString() == 'true') {

    globals.debuggingTool("will now be searching cause there are devices...");

    await profile_action.rebaseNodeAddresses(link: link);
  }

  for(String key in listOfDevice.keys) {

    if (children[key].getAttribute('@subscription').toString() == 'true'){

      globals.debuggingTool("subscription for $key started");

      await wemo_node.updateNodePointValues(link, key)
        .then((success) {

          globals.debuggingTool("subscription for ${key} ${(success) ? "successfull" : "failed"}");

        }).catchError((ex) {

          globals.debuggingTool("$ex");

        });
    }

    if (children[key].getAttribute('@posting').toString() == 'true'){

      globals.debuggingTool("posting for $key started");

      await wemo_node.httpUpdate(link, key)
        .then((success) {

          globals.debuggingTool("posting for ${key} ${(success) ? "successfull" : "failed"}");

        }).catchError((ex){

          globals.debuggingTool("${ex}");

        });
    }

  }
}

debuggingTool(var message){
  if(debuggingLog){
    print("DEBUG (${new DateTime.now()}) :: ${message}");
  }
}
