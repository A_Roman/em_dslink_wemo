
bool validIp(dynamic ipToCheck, {bool wildcard = false}) {

  bool isGoodIP = true;

  //if input in empty
  if (ipToCheck == null)               { isGoodIP = false; }
  else if ( ipToCheck is !String)      { isGoodIP = false; }
  else if (!ipToCheck.contains('.'))   { isGoodIP = false; }
  else {

    int section = 1;

    ipToCheck.split('.').forEach( (String ipSection) {

      int valueAsInt;

      try {
        valueAsInt = int.parse(ipSection);
      } catch (e) {
        valueAsInt = 1000;
      }

      if (valueAsInt > 255) { isGoodIP = false; }

      if (section == 3 && ipSection.toLowerCase() == 'x' && wildcard) {
        isGoodIP = true;
      }

      section++;
    });

  };

  return isGoodIP;
}

bool validIpRange( dynamic startIP, dynamic endIP) {

  bool subnetWildcard = false;

  // Verify network and subnet are properly set, including wildcard
  List<String> brokenStartIP = startIP.split('.');
  List<String> brokenEndIP = endIP.split('.');

  String startBase = brokenStartIP.sublist(0,3).join('.');
  String endBase = brokenEndIP.sublist(0,3).join('.');

  if (  startBase != endBase )
    { return false; }

  // if IP contain wildcard then work with the following
  if (startIP.contains('x') && endIP.contains('x')) {

    subnetWildcard = true;

    // if on contains it but not the other then exit now
  } else if (startIP.contains('x') && !endIP.contains('x') ||
      !startIP.contains('x') && endIP.contains('x') )
    { return false; }

  // Verify proper IP and if subnet check that its correct structure
  if ( !validIp( startIP, wildcard: subnetWildcard ) ||
      !validIp( endIP, wildcard: subnetWildcard))
    { return false; }

  return true;

}