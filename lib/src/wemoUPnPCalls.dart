//import "dart:async";
//import 'package:em_dslink_wemo/src/devicesForWemoLightSwitch.dart' as WemoDevice;
//import 'package:upnp/upnp.dart';
//import 'package:dslink/dslink.dart';

//////////////////////////
// SIMPLE COMMANDS
//////////////////////////

///
//Future<Service> _getSpecificService(Device device, String serviceName ) async {
//
//  return await device.getService('urn:Belkin:serviceId:${serviceName.toLowerCase()}1');
//
//}

/// unused with new webjob like structure
//Future<StateVariable> _getStateVariable(Device device, String serviceName, String stateVariableName) async {
//
//  Service service = await _getSpecificService(device, serviceName) ;
//
//  return await service.stateVariables.firstWhere((StateVariable variable) => variable.name == stateVariableName);
//
//}

///
//////////////////////////
// SUBSCIPTION
//////////////////////////

///
//class CustomStateSubscriptionManager extends StateSubscriptionManager {
//
//  String Ip;
//  String Port;
//
//  String Uuid;
//  String VariableName;
//
//  Device StoredDevice;
//
//  CustomStateSubscriptionManager(String ip, String port) {
//
//    Ip = ip;
//    Port = port;
//
//  }
//
//  Stream<dynamic> subscribeTo(String serviceName, String variableName) async* {
//
//    StoredDevice = await WemoDevice.getDevice(Ip, Port);
//
//
//  }
//
//}

//Stream<dynamic> Subscribe(String uuid, String serviceName, String variableName, String ipAddress, String port) async* {
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//  StateVariable stateVariable = await _getStateVariable(device, serviceName, variableName);
//
//  StateSubscriptionManager stateSubscriptionManager = new StateSubscriptionManager();
//
//  //has 2 min timeout after idling
//  await stateSubscriptionManager.init();
//
//  stateSubscriptionManager.subscribeToVariable(stateVariable).listen(( caugthValue) {
//
//    print('Log :: {Subscribe} Subscription picked up for $uuid with new state ${caugthValue}');
//    if ((caugthValue == 1 ||  caugthValue == 0))
//    {
//      link.val('/${uuid}/stateValue', caugthValue);
//
//      bool postingState;
//      try {
//        LocalNode lightswitchNode = link.getNode('/$uuid');
//        postingState = (lightswitchNode.getAttribute('@posting').toString() == "true" );
//      } catch (e) {
//        postingState = false;
//      }
//
//      if (postingState == true) {
//        wemo.httpPostCallFromPath('/$uuid', 'stateValue=${value}', link);
//      }
//
//      link.save();
//    }
//
//  }, onError: (e) {
//    print('Error :: {Subscribe} while subscribing to ${binaryVariable.name} for ${uuid}');
//
//  });
//  deviceSubscriptionsAndTimers[uuid]['subscriber'] = stateSubscriptionManager;
//
//}

////////////////////////

///
//Subscribe(String uuid, String serviceName, String variableName,
//    Map<String, dynamic> deviceSubscriptionsAndTimers, Map<String, dynamic> deviceToServicesMap,
//    LinkProvider link) async {
//
//  StateSubscriptionManager stateSubscriptionManager = new StateSubscriptionManager();
//
//  StateVariable binaryVariable = deviceToServicesMap[uuid][serviceName].stateVariables
//      .firstWhere((StateVariable variable) => variable.name == variableName);
//
//  //has 2 in timeout after idling
//  await stateSubscriptionManager.init();
//
//  stateSubscriptionManager.subscribeToVariable(binaryVariable).listen((value) {
//
//
//    print('Log :: {Subscribe} Subscription picked up for $uuid with new state ${value}');
//    if ((value == 1 ||  value == 0))
//    {
//      link.val('/${uuid}/stateValue', value);
//
//      bool postingState;
//      try {
//        LocalNode lightswitchNode = link.getNode('/$uuid');
//        postingState = (lightswitchNode.getAttribute('@posting').toString() == "true" );
//      } catch (e) {
//        postingState = false;
//      }
//
//      if (postingState == true) {
//        wemo.httpPostCallFromPath('/$uuid', 'stateValue=${value}', link);
//      }
//
//      link.save();
//    }
//
//  }, onError: (e) {
//    print('Error :: {Subscribe} while subscribing to ${binaryVariable.name} for ${uuid}');
//
//  });
//  deviceSubscriptionsAndTimers[uuid]['subscriber'] = stateSubscriptionManager;
//
//}

///
////////////////////////
// BASIC EVENT
////////////////////////

/// TODO : [Day3] [Toggle CurrentState] change to proper class
//Future<dynamic> toggleCurrentState(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
//  Service service = await _getSpecificService(device, 'basicevent');
//
//  Map<String,String> binaryResult = await GetState(ipAddress, port);
//
//  int toggleValue;
//
//  if (binaryResult['BinaryState'].toString() == '0') { toggleValue = 1; }
//  else { toggleValue = 0; };
//
////print('Current Result :: ${binaryResult['BinaryState']} to become :: $toggleValue');
//
////|---- SetBinaryState  is related to state value >BinaryState< has direction {IN}
////|---- Duration  is related to state value >Duration< has direction {IN}
////|---- EndAction  is related to state value >EndAction< has direction {IN}
////|---- UDN  is related to state value >UDN< has direction {IN}
////|---- CountdownEndTime  is related to state value >CountdownEndTime< has direction {OUT}
////|---- deviceCurrentTime  is related to state value >deviceCurrentTime< has direction {OUT}
//
//  response = await service.invokeAction('SetBinaryState', {
//    'BinaryState' : toggleValue
//  });
//
//  return response;
//}

//Future<dynamic> getFriendlyName(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
//  Service service = await _getSpecificService(device, 'basicevent');
//
////|---- FriendlyName  is related to state value >FriendlyName< has direction {IN}
//
//  response = await service.invokeAction('GetFriendlyName', {});
//
//  return response;
//}

///
/////////////////////
// FIRMWARE EVENT
/////////////////////

//Future<dynamic> getFirmwareVersion(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
//  Service service = await _getSpecificService(device, 'firmwareupdate');
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:firmwareupdate1');
//
////|---- FirmwareVersion  is related to state value >FirmwareVersion< has direction {OUT}
//
//  response = await service.invokeAction('GetFirmwareVersion', {});
//
//  return response;
//
//}

/// TODO : [Day2] [firmwareUpdate] change to class
//Future<dynamic> firmwareUpdate(String ipAddress, String port, String newFirmwareVersion, String releaseDate, String url, String signature) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:firmwareupdate1');
//
//  Service service = await _getSpecificService(device, 'firmwareupdate');
//
////|---- NewFirmwareVersion  is related to state value >NewFirmwareVersion< has direction {IN}
////|---- ReleaseDate  is related to state value >ReleaseDate< has direction {IN}
////|---- URL  is related to state value >URL< has direction {IN}
////|---- Signature  is related to state value >Signature< has direction {IN}
////|---- DownloadStartTime  is related to state value >DownloadStartTime< has direction {IN}
////|---- WithUnsignedImage  is related to state value >WithUnsignedImage< has direction {IN}
//
//  response = await service.invokeAction('StoreRules',
//      {
//        'NewFirmwareVersion' : newFirmwareVersion,
//        'ReleaseDate'        : releaseDate,
//        'URL'                : url,
//        'Signature'          : signature,
//        'DownloadStartTime'  : 0,
//        'WithUnsignedImage'  : ''
//      }
//  );
//
//  return response;
//
//}

///
///////////////////////
// RULES
///////////////////////

/// [Done]
//Future<dynamic> getRulesInfo(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:firmwareupdate1');
//
//  Service service = await _getSpecificService(device, 'rules');
//
////|---- ruleDbPath  is related to state value >ruleDbPath< has direction {OUT}
////|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {OUT}
////|---- errorInfo  is related to state value >errorInfo< has direction {OUT}
//
//  response = await service.invokeAction('FetchRules', {});
//
//  return response;
//
//}

/// [Done]
//Future<dynamic> storeRules(String ipAddress, String port, String ruleBody, String dbVersion, String proccesDB) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:firmwareupdate1');
//
//  Service service = await _getSpecificService(device, 'rules');
//
////|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {IN}
////|---- processDb  is related to state value >processDb< has direction {IN}
////|---- ruleDbBody  is related to state value >ruleDbBody< has direction {IN}
////|---- errorInfo  is related to state value >errorInfo< has direction {OUT}
//
//  response = await service.invokeAction('StoreRules',
//      {
//        'ruleDbVersion' : dbVersion,
//        'processDb'     : proccesDB,
//        'ruleDbBody'    : ruleBody
//      }
//  );
//
//  return response;
//
//}

///////////////////////
// GET INFO
///////////////////////

/// TODO : [Day3] [getMetaInfo] change to class, find use
//Future<dynamic> getMetaInfo(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:metainfo1');
//  Service service = await _getSpecificService(device, 'metainfo');
//
//
////|---- MetaInfo  is related to state value >MetaInfo< has direction {OUT}
//
//  response = await service.invokeAction('GetMetaInfo', {});
//
//  return response;
//
//}

/// TODO : [Day3] [getExtMetaInfo] change to class, find use
//Future<dynamic> getExtMetaInfo(String ipAddress, String port) async {
//
//  Map<String,String> response;
//
//  Device device = await WemoDevice.getDevice(ipAddress, port);
//
////  Service service = await device._getSpecificService('urn:Belkin:serviceId:metainfo1');
//  Service service = await _getSpecificService(device, 'metainfo');
//
////|---- ExtMetaInfo  is related to state value >ExtMetaInfo< has direction {OUT}
//
//  response = await service.invokeAction('GetExtMetaInfo', {});
//
//  return response;
//
//}

///
///////////////////////
// TIME SYNC
///////////////////////

/// TODO : [Day3] [timeSync] change to class, figure out proper use
/*
Future<dynamic> TimeSync(String ipAddress, String port, int epochTime, var timeZoneOffset, int dst, int dstSupported) async {

  Map<String,String> response;

  Device device = await WemoDevice.getDevice(ipAddress, port);

//  Service service = await device._getSpecificService('urn:Belkin:service:timesync:1');
  Service service = await _getSpecificService(device, 'timesync');

//|---- ExtMetaInfo  is related to state value >ExtMetaInfo< has direction {OUT}

  Map<String,dynamic> payload = {
    'UTC' : epochTime,
    'TimeZone' : timeZoneOffset,
    'dst' : dst,
    'DstSupported' :dstSupported
  };

  response = await service.invokeAction('TimeSync', payload );

  return response;

}
*/
