//import "dart:async";
//import 'package:async/async.dart';
//import "package:upnp/upnp.dart";
//import 'package:dslink_wemo/dslink_wemo.dart' as wemo;
//import 'package:dslink/dslink.dart';

/////////////////
//// CLASSES ////
/////////////////

class VacationManager {

  VacationManager(){

  }

}

Map<String, dynamic> _schedule = {
  'mon' : [],
  'tue' : [],
  'wed' : [],
  'thu' : [],
  'fri' : [],
  'sat' : [],
  'sun' : []
};

// will map dart days to wemo stringSchedule
Map<String, dynamic> _dayMapWemo = {
  'mon' : 1,
  'tue' : 2,
  'wed' : 3,
  'thu' : 4,
  'fri' : 5,
  'sat' : 6,
  'sun' : 7
};

/// This Class parses the

class InitialCustomEvent {

  String Name;
  DateTime StartTime;
  DateTime EndTime;
  int StateValue;

  int StartDayOfWeek;
  int EndDayOfWeek;

  /// this expects value of the following,
  /// "andreDay": {
  ///      "startDate":"2017-10-14T09:00:00",
  ///      "endDate":"2017-10-14T17:00:00",
  ///      "stateValue":"ON"
  ///    },
  /// regardless of initial length and whatnot

  InitialCustomEvent(String name, Map<String,dynamic> event) {

    Name = name;

    String start = event['startDate'];
    String end = event['endDate'];

    StateValue = event['stateValue'].toString().toLowerCase() == 'on' ? 1 : 0;

    if (start.contains('/')) {
      start = start.replaceAll('/', '-');
      end = end.replaceAll('/', '-');
    }

    StartTime = DateTime.parse(start);
    EndTime = DateTime.parse(end);

    StartDayOfWeek = StartTime.weekday;
    EndDayOfWeek = EndTime.weekday;

  }

  updateEndTime(DateTime newEnd) {
    EndTime = newEnd;
    EndDayOfWeek = EndTime.weekday;
  }

  updateStartTime(DateTime newStart) {
    StartTime = newStart;
    StartDayOfWeek = StartTime.weekday;
  }
}

class CustomEventDay {

  int DayOfWeek;
  DateTime StartTime;
  DateTime EndTime;

  /// this is only meant for 1 day events
  CustomEventDay(InitialCustomEvent customEvent) {

    StartTime = customEvent.StartTime;
    EndTime = customEvent.EndTime;

    DayOfWeek = customEvent.EndTime.weekday;

  }

}

class ThisWeekScheduleObject {

  Map<String,dynamic> DefaultWeeklySchedule;

  Map<String, dynamic> CustomWeeklySchedule = {
    'mon' : [],
    'tue' : [],
    'wed' : [],
    'thu' : [],
    'fri' : [],
    'sat' : [],
    'sun' : []
  };

  String TimeStructure;

//  schedule goes up to 11:59
  ThisWeekScheduleObject(String timeStructure, List<InitialCustomEvent> events, Map<String, dynamic> weeklySchedule) {

    DefaultWeeklySchedule = weeklySchedule;

    // monday is 1
    DateTime today = new DateTime.now();
    int dayNumber = today.weekday;
    

    // if not monday get monday and that Sunday
    int substractToMonday = dayNumber - 1;
    int addToSunday = 7 - dayNumber;

    DateTime mondayStart = new DateTime( today.year, today.month, (today.day-substractToMonday),0,0,0);
    DateTime sundayEnd = new DateTime( today.year, today.month, (today.day+addToSunday),23,59,59);

    List<InitialCustomEvent> listOfEventThisWeek = [];

    // will put all event in scope of this week if outside this week, cut it off to fit
    for (InitialCustomEvent event in events) {
      // adds if within week
      if ( event.StartTime.isAfter(mondayStart) && event.EndTime.isAfter(sundayEnd) ){

        listOfEventThisWeek.add(event);

      }
      // start is before monday
      else if ( !event.StartTime.isAfter(mondayStart) && event.EndTime.isAfter(sundayEnd) ){

        event.updateStartTime(mondayStart);
        listOfEventThisWeek.add(event);

      }
      // end if after sunday
      else if ( !event.StartTime.isAfter(mondayStart) && event.EndTime.isAfter(sundayEnd) ){

        event.updateEndTime(sundayEnd);
        listOfEventThisWeek.add(event);

      }
    }
    
    // will now break events that last longer than the day its in
    //TODO: needs to break cross day events

    List<CustomEventDay> eventsInSingleDays = [];

    int tempStartWeekDay = -1;
    int tempEndWeekDay = -1;
    bool checkIfDaysMatch = false;

    for (InitialCustomEvent eventInThisWeek in events) {

      // event start/end on same week day
      if (eventInThisWeek.StartTime.day == eventInThisWeek.EndTime.day) {
        eventsInSingleDays.add(new CustomEventDay(eventInThisWeek));
      } else {

      }

    }

    events.forEach( (InitialCustomEvent customEvent) =>
        eventsInSingleDays.add(new CustomEventDay(customEvent)) );

    List<CustomEventDay> todayCustomEvents = [];

    for (String day in DefaultWeeklySchedule.keys) {

      todayCustomEvents = eventsInSingleDays.where( (CustomEventDay customEventDay)
        => customEventDay.DayOfWeek == _dayMapWemo[day] ).toList();
      
    }

  }

}