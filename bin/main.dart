import 'package:em_dslink_wemo/src/profileActions.dart' as profile_action;
import 'package:em_dslink_wemo/src/wemoLightSwitchNode.dart' as wemo_node;
import 'package:em_dslink_wemo/src/globals.dart' as globals;

import 'package:dslink/dslink.dart';
import 'package:dslink/nodes.dart';

import 'dart:async';

LinkProvider link;

SimpleNodeProvider provider;
Requester requester;

SimpleNode devicesNode;

Map<String, dynamic> deviceToServicesMap = {};
Map<String, dynamic> deviceSubscriptionsAndTimers = {};

//TODO:[Day2] If needed will keep all wemo classes in memory
Map<String, wemo_node.WeMoNode> wemos = {};

main(List<String> args) async {

  Map<String, dynamic> nodeProfiles = {

    ///////////////////////////
    // Remove is always first!
    ///////////////////////////

    'remove'  : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> removeResponse =
        profile_action.remove(parentPath.path, link);

      return removeResponse;

    }),

    ///////////////////////////
    // Root Profiles!
    ///////////////////////////

    'discoverDevices' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> discoverResponse =
        await profile_action.discoverDevices(parentPath.path, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return discoverResponse;

    }),

    'searchInRange' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> searchInRangeResponse =
        await profile_action.searchInRange(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return searchInRangeResponse;

    }),

    'rebaseNodeAddresses' : (String path) ///////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> rebaseNodeAddressesResponse =
        await profile_action.rebaseNodeAddresses(link: link, params: params, pathOfParent: parentPath.path)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return rebaseNodeAddressesResponse;

    }),

    'toggleTimedDeviceDiscovery' : (String path) ///////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> toggleTimedDeviceDiscoveryResponse =
      await profile_action.toggleTimedDeviceDiscovery(link: link, params: params, pathOfParent: parentPath.path)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return toggleTimedDeviceDiscoveryResponse;

    }),

    'addDevice' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> addDeviceResponse =
        await profile_action.addDevice(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return addDeviceResponse;

    }),

    'changeRootDefaults' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> changeRootDefaultsResponse =
        await profile_action.changeRootDefaults(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return changeRootDefaultsResponse;

    }),

    'changeDMCredentials' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> changeRootDefaultsResponse =
        await profile_action.changeCredentials(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return changeRootDefaultsResponse;

    }),

    'testHttp' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> testHttpResponse =
      await profile_action.testHttp(parentPath.path, params, link)
        .catchError((error) {
          return {
            'succeeded' : false
          };
      });

      return testHttpResponse;

    }),


    'updateIpBatchSearchSize' : (String path) ////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> updateIpBatchSearchSizeResponse =
      await profile_action.updateIpBatchSearchSize(parentPath.path, params, link)
        .catchError((error) {
          return {
            'succeeded' : false
          };
      });

      return updateIpBatchSearchSizeResponse;

    }),

    'updatePostingRate' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> changePostRateResponse =
        await profile_action.updatePostingRate(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return changePostRateResponse;

    }),

    'updateSasTokenExpiration' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> updateSasTokenExpirationResponse =
      await profile_action.updateSasTokenExpiration(parentPath.path, params, link)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return updateSasTokenExpirationResponse;

    }),

    'changeRecoveryIpRange' : (String path) ////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> changeRecoveryIpRangeResponse =
        await profile_action.changeRecoveryIpRange(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return changeRecoveryIpRangeResponse;

    }),

    ///////////////////////////
    // WeMo Node Profiles!
    ///////////////////////////

    'getDeviceName' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      //TODO:[Day3] need to know if this is important or not
    }),

    'setTimeZone' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Setting TimeZone");

      //TODO: [Day3] need to figure out proper logic for settings TimeZone. For the time being relying on IP timezone

//      Map<String, dynamic> setTimeZoneResponse =
//          await profile_action.setTimeZone(parentPath.path, params, link);

      return {
        'succeeded' : true
      };

    }),

    'changeFriendlyName' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Changing Friendly Name");

      Map<String, dynamic> changeFriendlyNameResponse =
        await profile_action.changeFriendlyName(parentPath.path, params, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return changeFriendlyNameResponse;

    }),

    'getState' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Getting Switch State");

      Map<String, dynamic> getStateResponse =
        await profile_action.getState(parentPath.path, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return getStateResponse;

    }),

    'getSignalStrength' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Getting Signal Strength");

      Map<String, dynamic> getSignalStrengthResponse =
        await profile_action.getSignalStrength(parentPath.path, link)
          .catchError((error) {
            return {
              'succeeded' : false
            };
        });

      return getSignalStrengthResponse;

    }),

    'getFirmwareVersion' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Getting Firmware Version");

      Map<String, dynamic> getFirmwareVersionResponse =
      await profile_action.getFirmwareVersion(parentPath.path, link)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return getFirmwareVersionResponse;

    }),

    'firmwareUpdate' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) {

      globals.debuggingTool("Pushing firmware");

      //TODO : [Day2] need to add firmwareUpdate

    }),

    'storeSchedule' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Storing Schedule");

      Map<String, dynamic> storeScheduleResponse =
        await profile_action.storeSchedule(parentPath.path, params, link)
          .catchError( (error) {
            return{
              'succeeded' : false
            };
        });

      return storeScheduleResponse;

    }),

    'updateSchedule' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Updating Schedule");

      Map<String, dynamic> updateScheduleResponse =
        await profile_action.updateSchedule(parentPath.path, params, link)
          .catchError( (error) {
            return{
              'succeeded' : false
            };
        });

      return updateScheduleResponse;

    }),

    'storeCustomEvents' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Storing Custom Events");

      Map<String, dynamic> storeCustomEventsResponse =
        await profile_action.storeCustomEvents(parentPath.path, params, link)
          .catchError( (error) {
            return{
              'succeeded' : false
            };
        });

      return storeCustomEventsResponse;

    }),

    'toggleSubscription' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> toggleSubscriptionResponse =
        await profile_action.toggleSubscription(parentPath.path, params, link)
          .catchError( (error) {
            return{
              'succeeded' : false
            };
        });

      return toggleSubscriptionResponse;

    }),

    'togglePosting' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> togglePostingResponse =
      await profile_action.togglePosting(parentPath.path, params, link)
          .catchError( (error) {
        return{
          'succeeded' : false
        };
      });

      return togglePostingResponse;

    }),

    'publisherSettings' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      globals.debuggingTool("Taking in publisher settings");

      Map<String, dynamic> publisherSettingsResponse =
        await profile_action.publisherSettings(parentPath.path, params, link)
          .catchError( (error) {
            return{
              'succeeded' : false
            };
        });

      return publisherSettingsResponse;

    }),

  };

  Map<String, dynamic> rootNodeDefinitions = {
    r'$name'                : 'WeMo DSLink Root',
    r'$is'                  : 'DSLink',

    r'$defaultDMUser'       : 'user123',
    r'$$defaultDMPassword'  : 'User12345',

    '@inDiscovery'            : false,
    '@timedDiscovery'         : false,

    '@defaultPorts'           : ['49153','49154','49155'],
    '@defaultSubnets'         : ['1','2','3'],
    '@defaultDeviceManager'   : 'https://EMDev.azure-devices.net/devices/|DeviceID|/messages/events?api-version=2016-11-14',

    '@postRateInMin'          : 10,
    '@sasTokenExpirationInMin': 5,

    '@recoveryIpRange'        : ['192.168.3.2','192.168.3.15'],
    '@ipBatchSearchSize'      : 50,


    // discover devices action
    'discoverDevices'     : {
      r'$name'        : 'Discover Devices',
      r'$invokable'   : 'config',

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'numberFound',
          'type'    : 'number'
        },
        {
          "name"    : 'numberAdded',
          "type"    : 'number'
        }
      ],
      r'$is'          : 'discoverDevices',
    },

    // search for devices in range of IPs
    'searchInRange'       : {
      r'$name'        : 'Search in Range',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'startIP',
          'type'    : 'string',
          'default' : '192.168.3.1'
        },
        {
          'name'    : 'endIP',
          'type'    : 'string',
          'default' : '192.168.3.15'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'numberFound',
          'type'    : 'number'
        },
        {
          "name"    : 'numberAdded',
          "type"    : 'number'
        },
        {
          "name"    : 'succeeded',
          "type"    : 'bool'
        }
      ],

      r'$is'          : 'searchInRange'
    },

    // look at existing devices and update nodes if changes
    'rebaseNodeAddresses' : {
      r'$name'        : 'Rebase Node Addresses',
      r'$invokable'   : 'config',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'devicesFound',
          'type' : 'int'
        },
        {
          'name' : 'nodesUpdated',
          'type' : 'int'
        },
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is'          : 'rebaseNodeAddresses'
    },

    // add device from specific ip
    'addDevice'           : {
      r'$name'        : 'Add Device',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'ip',
          'type'    : 'string',
          'default' : '192.168.3.4'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'addDevice'
    },

    // (changes attributes) update wemo defaults
    'changeRootDefaults'  : {
      r'$name'        : 'Change Root Defaults',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'defaultPorts',
          'type'    : 'array',
          'default' : ['49153','49154','49155']
        },
        {
          'name'    : 'defaultSubnets',
          'type'    : 'array',
          'default' : ['1','2','3']
        },
        {
          'name'    : 'defaultDeviceManager',
          'type'    : 'string',
          'default' : 'https://emcoredevicemanagementapidev.azurewebsites.net/api/dsa/wemo/pushschedule'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'changeRootDefaults'
    },

    // {change attributes} will toggle if device discovery is done on scheduled subscription/
    'toggleTimedDeviceDiscovery' : {

      r'$name'        : 'Toggle Timed Device Discovery',
      r'$invokable'   : 'config',

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],
      r'$is'          : 'toggleTimedDeviceDiscovery',

    },

    // (change attributes) update postRate
    'updatePostingRate'  : {
      r'$name'        : 'Change Post Rate',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'postRateInMin',
          'type'    : 'number'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'updatePostingRate'
    },

    // (change attributes) update length of SaS token expiration
    'updateSasTokenExpiration'  : {
      r'$name'        : 'Change SaS Token Expiration',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'sasTokenExpirationInMin',
          'type'    : 'number'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'updateSasTokenExpiration'
    },

    // (change attributes) update postRate
    'updateIpBatchSearchSize'  : {
      r'$name'        : 'Change IP Batch Size',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'ipBatchSearchSize',
          'type'    : 'number'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'updateIpBatchSearchSize'
    },

    // {change attributes} change the IP range for device recovery
    'changeRecoveryIpRange'  : {
      r'$name'        : 'Change Recovery IP Rate',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'startIp',
          'type'    : 'string',
          'default' : '192.168.3.1'
        },
        {
          'name'    : 'endIp',
          'type'    : 'string',
          'default' : '192.168.3.15'
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'changeRecoveryIpRange'
    },

    // (changes attributes) update device management credentials defaults
    'changeDefaultDMCredentials'  : {
      r'$name'        : 'Change Default Device Management Credentials',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'defaultDMUser',
          'type'    : 'string',
        },
        {
          'name'    : 'defaultDMPassword',
          'type'    : 'string',
          'editor'  : 'password',
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        }
      ],

      r'$is'          : 'changeDMCredentials'
    },

    // test http connection to anything
    'testHttp'  : {
      r'$name'        : 'Test Http Connection',
      r'$invokable'   : 'config',

      r'$params'      : [
        {
          'name'    : 'targetUri',
          'type'    : 'string',
        },
        {
          'name'    : 'headers',
          'type'    : 'map',
        },
        {
          'name'    : 'body',
          'type'    : 'string',
        }
      ],

      r'$result'      : 'values',
      r'$columns'     : [
        {
          'name'    : 'succeeded',
          'type'    : 'bool'
        },
        {
          'name'    : 'statusCode',
          'type'    : 'number'
        }
      ],

      r'$is'          : 'testHttp'
    },

  };

  link = new LinkProvider(args
    , "WeMo_LightSwitch-"
    , isResponder: true
    , isRequester: true
    , encodePrettyJson: true
    , defaultNodes: rootNodeDefinitions
    , profiles: nodeProfiles
  );

  provider = link.provider;
  requester = link.requester;

  link.init();


  link.connect().whenComplete(() async {

    localPostingJob();

  });

}

// timer will run every x minutes and push data accordingly (one timer to rule them all!@)
localPostingJob() async {

  LocalNode lNode = link.getNode('/');
  double postingTime = double.parse(lNode.get('@postRateInMin').toString());
  int postingTimeInSeconds = (postingTime*60).floor();

  globals.debuggingTool('webjob initialized every ${postingTimeInSeconds} seconds on boot');

  var tempTimerDuration = new Duration(seconds: postingTimeInSeconds);

  globals.timer = new Timer.periodic(tempTimerDuration, (_) async {
    await globals.updateFunction(link);
  });

}

